package com.yzq.os.spider.v.service.domain;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yzq.os.spider.v.dao.SpiderTaskDao;
import com.yzq.os.spider.v.domain.SpiderTask;

@Service
public class SpiderTaskService {

	@Autowired
	private SpiderTaskDao crawlTaskDao;

	public int save(SpiderTask crawlTask) {
		return crawlTaskDao.save(crawlTask);
	}

	public void update(SpiderTask crawlTask) {
		crawlTaskDao.update(crawlTask);
	}

	public List<SpiderTask> findLocalTasks(List<String> localIps) {
		return crawlTaskDao.findLocalTasks(localIps);
	}

	public List<SpiderTask> list() {
		return crawlTaskDao.find();
	}

	public SpiderTask loadById(int id) {
		return crawlTaskDao.load(id);
	}

	public void delete(int id) {
		crawlTaskDao.delete(id);
	}

}
