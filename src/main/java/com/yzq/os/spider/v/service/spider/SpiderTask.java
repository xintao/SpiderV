package com.yzq.os.spider.v.service.spider;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import com.yzq.os.spider.v.domain.Record;
import com.yzq.os.spider.v.domain.ListPageConfig;
import com.yzq.os.spider.v.domain.QueryURL;
import com.yzq.os.spider.v.domain.SearchEngine;
import com.yzq.os.spider.v.service.domain.SpiderRecordService;
import com.yzq.os.spider.v.service.domain.QueryURLService;
import com.yzq.os.spider.v.service.domain.SearchEngineParamService;
import com.yzq.os.spider.v.service.domain.SearchEngineService;
import com.yzq.os.spider.v.service.http.HttpClientService;

/**
 * 抓取任务类（多线程任务）
 * 
 * @author 苑志强(xingyu_yzq@163.com)
 * 
 */
public interface SpiderTask extends Runnable {
	/**
	 * 设置搜索引擎
	 * 
	 * @param searchEngineService
	 */
	void setSearchEngineService(SearchEngineService searchEngineService);

	/**
	 * 设置搜索URL服务类
	 * 
	 * @param queryURLService
	 */
	void setQueryURLService(QueryURLService queryURLService);

	/**
	 * 设置抓取服务方法
	 * 
	 * @param spiderRecordService
	 */
	void setSpiderRecordService(SpiderRecordService spiderRecordService);

	/**
	 * 设置搜索引擎
	 * 
	 * @param searchEngine
	 */
	void setSearchEngine(SearchEngine searchEngine);

	/**
	 * 设置搜索引擎参数
	 * 
	 * @param searchEngineParamService
	 */
	void setSearchEngineParamService(
			SearchEngineParamService searchEngineParamService);

	/**
	 * 设置列表数据页配置
	 * 
	 * @param listPageConfig
	 */
	void setListPageConfig(ListPageConfig listPageConfig);

	/**
	 * 设置搜索URL
	 * 
	 * @param queryURL
	 */
	void setQueryURL(QueryURL queryURL);

	/**
	 * 设置抓取时间
	 * 
	 * @param crawlDate
	 */
	void setCrawlDate(Date crawlDate);

	/**
	 * 设置抓取数据保存表名
	 * 
	 * @param tableName
	 */
	void setTableName(String tableName);

	/**
	 * 设置HTTP请求服务类
	 * 
	 * @param httpClientService
	 */
	void setHttpClientService(HttpClientService httpClientService);

	/**
	 * 抓取前置处理器
	 */
	void initializationBeforeRun();

	/**
	 * 抓取前重新装饰搜索URL
	 */
	void reWriteQueryUrl();

	/**
	 * 抓取搜索URL的HTML代码
	 * 
	 * @return
	 * @throws Exception
	 */
	String crawlPostUrlHtmlSource() throws Exception;

	/**
	 * 设置HTML代码
	 * 
	 * @param postUrlHtmlSource
	 */
	void setPostUrlHtmlSource(String postUrlHtmlSource);

	/**
	 * 页面是否是“没有搜索结果”的页面
	 * 
	 * @return
	 */
	boolean isNoSearchResultsPage();

	/**
	 * 数据列表页面是否是第一页
	 * 
	 * @return
	 */
	boolean isFirstListPage();

	/**
	 * 获取列表数据页面中的实际返回记录数
	 * 
	 * @return
	 */
	Integer extractReturnRecordNum();

	/**
	 * 是否超出最大显示记录数
	 * 
	 * @return
	 */
	Boolean largeThanMaxReturn();

	/**
	 * 添加新的搜索条件从而减少搜索结果集
	 * 
	 * @param queryURL
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	List<QueryURL> generateQualifiedURLs(QueryURL queryURL)
			throws UnsupportedEncodingException;

	/**
	 * 选择包含数据列表部分的HTML代码
	 * 
	 * @return
	 */
	String sectionContainsJobsHtmlSource();

	/**
	 * 设置包含数据列表部分的HTML代码
	 * 
	 * @param containsJobsHtmlSource
	 */
	void setContainsJobsHtmlSource(String containsJobsHtmlSource);

	/**
	 * 通过搜索URL参数标记数据类型
	 * 
	 * @return
	 */
	String extractJobTypeCode();

	/**
	 * 通过搜索URL参数标记数据类型
	 * 
	 * @return
	 */
	String extractIndustryCode();

	/**
	 * 通过搜索URL参数标记数据类型
	 * 
	 * @return
	 */
	String extractCityCode();

	/**
	 * 从列表数据HTML代码中抽取列表数据对象
	 * 
	 * @return
	 */
	List<Record> extractJobs();

	/**
	 * 将列表数据保存到数据库（批量保存）
	 * 
	 * @param records
	 */
	void batchSaveJobInfos(List<Record> records);

	/**
	 * 获取当前页码
	 * 
	 * @return
	 */
	Integer extractCurrentPageNo();

	/**
	 * 是否存在下一页
	 * 
	 * @return
	 */
	Boolean hasNextPage();

	/**
	 * 生成下一页搜索URL
	 * 
	 * @return
	 */
	String makeNextSpellUrl();

	/**
	 * 生成从下一页开始到最后一页的全部搜索URL
	 * 
	 * @param totalPageNum
	 * @return
	 */
	List<String> makeSecondStartNextSpellUrl(int totalPageNum);

	/**
	 * 通过URL字符串创建搜索URL对象
	 * 
	 * @param spellUrl
	 * @return
	 */
	QueryURL createQueryURL(String spellUrl);

	/**
	 * 是否使用强制计算翻页标识
	 * 
	 * @param status
	 */
	void setForceUseCalculateTurnPage(boolean status);

	/**
	 * 获取计算翻页标识
	 * 
	 * @return
	 */
	boolean getForceUseCalculateTurnPage();

}
