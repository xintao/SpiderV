package com.yzq.os.spider.v.domain;

/**
 * 网站对象
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class WebSite implements Comparable<WebSite> {
	private int id;
	private String name;

	public WebSite(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	@Override
	public int compareTo(WebSite o) {
		int returnValue = 0;
		if (name != null && o.getName() != null) {
			if (name.compareTo(o.getName()) > 0) {
				returnValue = 1;
			} else if (name.compareTo(o.getName()) == 0) {
				returnValue = 0;
			} else {
				returnValue = -1;
			}
		}
		return returnValue;
	}

}
