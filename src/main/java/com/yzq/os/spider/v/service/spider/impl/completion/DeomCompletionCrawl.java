package com.yzq.os.spider.v.service.spider.impl.completion;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.yzq.os.spider.v.domain.QueryURL;
import com.yzq.os.spider.v.service.spider.AbstractCompletionCrawl;
import com.yzq.os.spider.v.util.EncodeUtil;
import com.yzq.os.spider.v.util.QuanBanTransform;

public class DeomCompletionCrawl extends AbstractCompletionCrawl {

	private static Logger logger = Logger.getLogger(DeomCompletionCrawl.class);

	@Override
	public void doCompletion() throws Exception {
		int newUrlCount = 0;
		logger.info(EncodeUtil.gbk2iso("Begin run completion for engine :["
				+ searchEngine.getName() + "]"));
		List<String> allCompanyNames = crawlJobService
				.findAllCompanyNames(jobSaveTableName);
		if (CollectionUtils.isNotEmpty(allCompanyNames)) {
			List<QueryURL> queryUrls = new ArrayList<QueryURL>();
			String urlPattren = searchEngine.getBaseUrl()
					+ "?companyName=<COMPAN_NAME_ENCODE>";
			for (String companyName : allCompanyNames) {
				if (StringUtils.isNotBlank(companyName)) {
					companyName = QuanBanTransform.quan2ban(companyName);
					String url = StringUtils.replace(urlPattren,
							"<COMPAN_NAME_ENCODE>",
							URLEncoder.encode(companyName, "UTF-8"));
					queryUrls.add(new QueryURL(searchEngineId, url, url));
				}
			}
			newUrlCount = CollectionUtils.size(queryUrls);
			queryURLService.batchSave(searchEngineId, queryUrls, 1000);
		} else {
			logger.error("All companynames is empty.");
		}
		logger.info(EncodeUtil.gbk2iso("End run completion for engine :["
				+ searchEngine.getName() + "] newUrlCount:[" + newUrlCount
				+ "]"));
	}

}
