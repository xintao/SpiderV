package com.yzq.os.spider.v.service.spider;

import java.util.Date;

import com.yzq.os.spider.v.domain.SearchEngine;
import com.yzq.os.spider.v.service.domain.SpiderRecordService;
import com.yzq.os.spider.v.service.domain.QueryURLService;

/**
 * 抽象数据补全类
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public abstract class AbstractCompletionCrawl implements CompletionCrawl {

	protected SpiderRecordService crawlJobService;
	protected QueryURLService queryURLService;
	protected SearchEngine searchEngine;
	protected int searchEngineId;
	protected Date markDate;
	protected String jobSaveTableName;

	@Override
	public void setCrawlJobService(SpiderRecordService crawlJobService) {
		this.crawlJobService = crawlJobService;
	}

	@Override
	public void setSearchEngineId(int searchEngineId) {
		this.searchEngineId = searchEngineId;
	}

	@Override
	public void setMarkDate(Date markDate) {
		this.markDate = markDate;
	}

	@Override
	public void setJobSaveTableName(String jobSaveTableName) {
		this.jobSaveTableName = jobSaveTableName;
	}

	@Override
	public void setSearchEngine(SearchEngine searchEngine) {
		this.searchEngine = searchEngine;
	}

	@Override
	public void setQueryURLService(QueryURLService queryURLService) {
		this.queryURLService = queryURLService;
	}

}
