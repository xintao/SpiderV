package com.yzq.os.spider.v.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

/**
 * 公共控制器类
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class CommonController {
	
	/**
	 * 向客户端响应XML文件
	 * @param response
	 * @param fileName
	 * @param xml
	 * @throws IOException
	 */
	protected void responseXml(HttpServletResponse response, String fileName,
			String xml) throws IOException {
		response.setCharacterEncoding("UTF-8");
		response.setContentType("Content-Type: text/xml");
		response.setHeader("Content-disposition", "attachment; filename=" + fileName);
		PrintWriter writer = response.getWriter();
		writer.write(xml);
		writer.flush();
		IOUtils.closeQuietly(writer);
	}
}
