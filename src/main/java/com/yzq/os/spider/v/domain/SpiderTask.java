package com.yzq.os.spider.v.domain;

/**
 * 抓取任务对象
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class SpiderTask {
	private Integer id;
	/**
	 * 运行服务器IP地址
	 */
	private String runServerIp;
	
	/**
	 * 搜索引擎ID
	 */
	private int searchEngineId;
	/**
	 * 搜索引擎名称
	 */
	private String searchEngineName;
	
	/**
	 * 每次领取的抓取URL数量
	 */
	private int findUrlSize;
	
	/**
	 * 执行抓取的最小线程数量
	 */
	private int minThreadNum;
	
	/**
	 * 执行抓取的最大线程数量
	 */
	private int maxThreadNum;
	
	/**
	 * 线程池任务队列长度
	 */
	private int poolQueueSize;
	
	/**
	 * 自动运行时间表达式
	 */
	private String cronExp;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRunServerIp() {
		return runServerIp;
	}

	public void setRunServerIp(String runServerIp) {
		this.runServerIp = runServerIp;
	}

	public int getSearchEngineId() {
		return searchEngineId;
	}

	public void setSearchEngineId(int searchEngineId) {
		this.searchEngineId = searchEngineId;
	}

	public String getSearchEngineName() {
		return searchEngineName;
	}

	public void setSearchEngineName(String searchEngineName) {
		this.searchEngineName = searchEngineName;
	}

	public int getFindUrlSize() {
		return findUrlSize;
	}

	public void setFindUrlSize(int findUrlSize) {
		this.findUrlSize = findUrlSize;
	}

	public int getMinThreadNum() {
		return minThreadNum;
	}

	public void setMinThreadNum(int minThreadNum) {
		this.minThreadNum = minThreadNum;
	}

	public int getMaxThreadNum() {
		return maxThreadNum;
	}

	public void setMaxThreadNum(int maxThreadNum) {
		this.maxThreadNum = maxThreadNum;
	}

	public int getPoolQueueSize() {
		return poolQueueSize;
	}

	public void setPoolQueueSize(int poolQueueSize) {
		this.poolQueueSize = poolQueueSize;
	}

	public String getCronExp() {
		return cronExp;
	}

	public void setCronExp(String cronExp) {
		this.cronExp = cronExp;
	}

	@Override
	public String toString() {
		return "CrawlTask [id=" + id + ", runServerIp=" + runServerIp + ", searchEngineId=" + searchEngineId + ", findUrlSize=" + findUrlSize + ", minThreadNum=" + minThreadNum + ", maxThreadNum=" + maxThreadNum + ", poolQueueSize=" + poolQueueSize + ", cronExp=" + cronExp + "]";
	}

}
