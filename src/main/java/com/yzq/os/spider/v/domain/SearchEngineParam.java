package com.yzq.os.spider.v.domain;

/**
 * 目标网站搜索参数配置
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class SearchEngineParam {
	private Integer id;
	/**
	 * 搜索引擎ID
	 */
	private int searchEngineId;
	
	/**
	 * 参数名称
	 */
	private String name;
	
	/**
	 * 参数值
	 */
	private String value;
	/**
	 * 是否单值参数
	 */
	private int singleValue;// 0:no 1:yes
	/**
	 * 是否每次必须提交的参数（必要）
	 */
	private int required;// 0:no 1:yes
	/**
	 * 排序值，影响拼入URL的顺序
	 */
	private int orderNum;
	
	/**
	 * 描述
	 */
	private String desc;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getSearchEngineId() {
		return searchEngineId;
	}

	public void setSearchEngineId(int searchEngineId) {
		this.searchEngineId = searchEngineId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getRequired() {
		return required;
	}

	public void setRequired(int required) {
		this.required = required;
	}

	public int getSingleValue() {
		return singleValue;
	}

	public void setSingleValue(int singleValue) {
		this.singleValue = singleValue;
	}

	public int getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
