package com.yzq.os.spider.v;

import org.springframework.context.ApplicationContext;

/**
 * 应用常量类
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public final class Constants {
	/**
	 * 包含日期和时间的格式化模式
	 */
	public static final String DATE_WITH_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
	/**
	 * 包含日期的格式化模式
	 */
	public static final String DATE_PATTERN = "yyyy-MM-dd";
	/**
	 * 是否使用代理访问网咯
	 */
	public static final boolean USE_PROXY_SERVER = false;

	/**
	 * 代理服务器地址
	 */
	public static final String PROXY_SERVER_HOST = "192.168.1.1";
	/**
	 * 代理服务器端口
	 */
	public static final int PROXY_SERVER_PORT = 3128;
	/**
	 * Spring 上下文对象
	 */
	private static ApplicationContext applicationContext;

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public static void setApplicationContext(
			ApplicationContext applicationContext) {
		Constants.applicationContext = applicationContext;
	}

}
