package com.yzq.os.spider.v.util;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.oro.text.regex.MalformedPatternException;
import org.apache.oro.text.regex.MatchResult;
import org.apache.oro.text.regex.Pattern;
import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.PatternMatcher;
import org.apache.oro.text.regex.PatternMatcherInput;
import org.apache.oro.text.regex.Perl5Compiler;
import org.apache.oro.text.regex.Perl5Matcher;
import org.apache.oro.text.regex.Perl5Substitution;
import org.apache.oro.text.regex.Util;

/**
 * 正则工具类
 * 
 * @author 苑志强(xingyu_yzq@163.com)
 * 
 */
public final class Regex {

	private static Logger log = Logger.getLogger(Regex.class);

	private Regex() {
	}

	/**
	 * 匹配单行单域数据
	 * 
	 * @param str
	 * @param regex
	 * @param isFull
	 * @return
	 */
	public static String matchSRowSField(String str, String regex,
			boolean isFull) {
		String returnValue = null;
		try {
			List<MatchResult> resultList = matcher(str, regex);
			if (resultList.size() >= 1) {
				MatchResult result = (MatchResult) resultList.get(0);
				if (isFull == true) {
					returnValue = result.group(0);
				} else {
					returnValue = result.group(1);
				}
			}
		} catch (Exception e) {
			log.error("", e);
		}
		return returnValue;
	}

	/**
	 * 匹配单行多域数据
	 * 
	 * @param str
	 * @param regex
	 * @return
	 */
	public static ResultSet matchSRowMField(String str, String regex) {
		List<MatchResult> resultList = matcher(str, regex);
		if (resultList.size() >= 1) {
			MatchResult result = (MatchResult) resultList.get(0);
			ResultSet rs = new ResultSet();
			for (int j = 1; j < result.groups(); j++) {
				rs.add(result.group(j));
			}
			return rs;
		}
		return null;
	}

	/**
	 * 匹配多行单域数据
	 * 
	 * @param str
	 * @param regex
	 * @param isFull
	 * @return
	 */
	public static List<String> matchMRowSField(String str, String regex,
			boolean isFull) {
		List<MatchResult> resultList = matcher(str, regex);

		LinkedList<String> list = new LinkedList<String>();

		for (int i = 0; i < resultList.size(); i++) {
			MatchResult result = (MatchResult) resultList.get(i);
			if (isFull == true) {
				list.add(result.group(0));
			} else {
				list.add(result.group(1));
			}
		}
		return list;
	}

	/**
	 * 匹配多行多域数据
	 * 
	 * @param str
	 * @param regex
	 * @param isFull
	 * @return
	 */
	public static List<ResultSet> matchMRowMField(String str, String regex,
			boolean isFull) {
		List<MatchResult> resultList = matcher(str, regex);

		LinkedList<ResultSet> resultSetObjList = new LinkedList<ResultSet>();

		for (int i = 0; i < resultList.size(); i++) {
			MatchResult result = (MatchResult) resultList.get(i);
			ResultSet rs = new ResultSet();
			if (isFull == true) {
				rs.add(result.group(0));
			}
			for (int j = 1; j < result.groups(); j++) {
				rs.add(result.group(j));
			}
			resultSetObjList.add(rs);
		}
		return resultSetObjList;
	}

	/**
	 * 正则匹配
	 * 
	 * @param str
	 * @param regex
	 * @return
	 */
	public static List<MatchResult> matcher(String str, String regex) {
		Pattern pattern = null;

		PatternCompiler com = new Perl5Compiler();
		LinkedList<MatchResult> resultList = new LinkedList<MatchResult>();

		try {
			pattern = com.compile(regex, Perl5Compiler.CASE_INSENSITIVE_MASK);
		} catch (MalformedPatternException e) {
			e.printStackTrace();
		}
		PatternMatcherInput input = new PatternMatcherInput(str);
		PatternMatcher matcher = new Perl5Matcher();

		while (matcher.contains(input, pattern)) {
			resultList.add(matcher.getMatch());
		}

		return resultList;
	}

	/**
	 * 替换字符串
	 * 
	 * @param str
	 * @param reg
	 * @param replaceStr
	 * @param replacecounter
	 * @return
	 */
	public static String replace(String str, String reg, String replaceStr,
			int replacecounter) {
		String result = str;

		Pattern pattern = null;

		if (replacecounter == 0) {
			replacecounter = Util.SUBSTITUTE_ALL;
		}
		String _str = str;
		String _reg = reg;

		PatternCompiler com = new Perl5Compiler();
		try {
			pattern = com.compile(_reg, Perl5Compiler.CASE_INSENSITIVE_MASK);
		} catch (MalformedPatternException e) {
			e.printStackTrace();
		}
		PatternMatcher matcher = new Perl5Matcher();
		result = Util.substitute(matcher, pattern, new Perl5Substitution(
				replaceStr), _str, replacecounter);
		return result;
	}

	/**
	 * 替换参数值
	 * 
	 * @param _str
	 * @param param
	 * @param value
	 * @return
	 */
	public static String replaceParamValue(String _str, String param,
			String value) {
		return _str.replaceAll("\\{\\$" + param + "\\}", value);
	}

	/**
	 * 替换多个参数值（参数名和值通过Map包装）
	 * 
	 * @param _str
	 * @param paramMap
	 * @return
	 */
	public static String replaceParamValue(String _str,
			Map<String, String> paramMap) {
		String str = _str;
		Iterator<String> it = paramMap.keySet().iterator();
		while (it.hasNext()) {
			String param = it.next();
			String value = paramMap.get(param);
			str = str.replaceAll("\\{\\$" + param + "\\}", value);
		}
		return str;
	}

	/**
	 * 判断支付串是否匹配指定的正则集合，匹配任意返回true
	 * 
	 * @param regex
	 * @param str
	 * @return
	 */
	public static boolean isMatchAny(String regex[], String str) {
		boolean returnValue = false;
		if (regex != null && regex.length > 0) {
			for (String reg : regex) {
				if (isMatch(reg, str)) {
					returnValue = true;
					break;
				}
			}
		}
		return returnValue;
	}

	/**
	 * 判断支付串是否匹配指定的正则
	 * 
	 * @param regex
	 * @param str
	 * @return
	 */
	public static boolean isMatch(String regex, String str) {
		if (regex == null || str == null)
			return false;

		Pattern pattern = null;
		PatternCompiler com = new Perl5Compiler();

		try {
			pattern = com.compile(regex, Perl5Compiler.CASE_INSENSITIVE_MASK);
		} catch (MalformedPatternException e) {
			e.printStackTrace();
		}

		PatternMatcher matcher = new Perl5Matcher();
		return matcher.matches(str, pattern);
	}

	/**
	 * 匹配前缀
	 * 
	 * @param regex
	 * @param str
	 * @return
	 */
	public static boolean isMatchesPrefix(String regex, String str) {
		if (regex == null || str == null)
			return false;

		Pattern pattern = null;
		PatternCompiler com = new Perl5Compiler();

		try {
			pattern = com.compile(regex, Perl5Compiler.CASE_INSENSITIVE_MASK);
		} catch (MalformedPatternException e) {
			e.printStackTrace();
		}

		PatternMatcher matcher = new Perl5Matcher();
		return matcher.matchesPrefix(str, pattern);
	}

	/**
	 * 部分匹配
	 * @param regex
	 * @param str
	 * @return
	 */
	public static boolean isContains(String regex, String str) {
		if (regex == null || str == null)
			return false;

		Pattern pattern = null;
		PatternCompiler com = new Perl5Compiler();

		try {
			pattern = com.compile(regex, Perl5Compiler.CASE_INSENSITIVE_MASK);
		} catch (MalformedPatternException e) {
			e.printStackTrace();
		}

		PatternMatcher matcher = new Perl5Matcher();
		return matcher.contains(str, pattern);
	}

}