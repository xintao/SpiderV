<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<link href="<c:url value="/css/ui-lightness/jquery-ui-1.8.6.custom.css"/>" rel="stylesheet" type="text/css"  />
<script src="<c:url value="/js/jquery-1.4.2.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/jquery-ui-1.8.6.custom.min.js"/>" type="text/javascript"></script>
<title>
	<c:if test="${formType == 'truncate' }">
		Truncate搜索URL表
	</c:if>
	<c:if test="${formType == 'initValid' }">
		InsertInto经过验证的搜索URL
	</c:if>
	<c:if test="${formType == 'deleteValid' }">
		从备份表中删除URL
	</c:if>
</title>
<script type="text/javascript">
	function submitConfirm(){
		var message;
		<c:if test="${formType == 'truncate' }">
			message="注意：您将执行truncate表操作，确认吗？";
		</c:if>
		<c:if test="${formType == 'initValid' }">
			message="注意：您将执行备份表中数据导入操作，确认吗？";
		</c:if>
		<c:if test="${formType == 'deleteValid' }">
			message="注意：您将执行从备份表中删除URL操作，确认吗？";
		</c:if>
		return confirm(message);
	}
</script>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>

<form action="
	<c:if test="${formType == 'truncate' }">
		<c:url value="/admin/queryurl/truncate"/>
	</c:if>
	<c:if test="${formType == 'initValid' }">
		<c:url value="/admin/queryurl/initValid"/>
	</c:if>
	<c:if test="${formType == 'deleteValid' }">
		<c:url value="/admin/queryurl/deleteValid"/>
	</c:if>
" method="post" onsubmit="return submitConfirm()" >
	<table>
		<tr>
			<td colspan="3" class="tableHeadBg">
				<div align="center">
					<c:if test="${formType == 'truncate' }">
						Truncate搜索URL表
					</c:if>
					<c:if test="${formType == 'initValid' }">
						InsertInto经过验证的搜索URL
					</c:if>
					<c:if test="${formType == 'deleteValid' }">
						从备份表中删除URL
					</c:if>
				</div>
			</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">搜索引擎:</td>
			<td>
				<select name="searchEngineId">
					<c:forEach var="engine" items="${engines}">
						<option value="${engine.id}">${engine.name}</option>
					</c:forEach>
				</select>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td>&nbsp;</td>
			<td>
				<input type="submit" value="提交"><input type="reset" value="重置">
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>
<br><a href="<c:url value="/index.jsp" />">返回首页</a><br>
</body>
</html>
