<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<link href="<c:url value="/css/ui-lightness/jquery-ui-1.8.6.custom.css"/>" rel="stylesheet" type="text/css"  />
<script src="<c:url value="/js/jquery-1.4.2.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/jquery-ui-1.8.6.custom.min.js"/>" type="text/javascript"></script>
<title>管理抓取任务执行计划</title>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>
<form action="<c:url value="/admin/task"/>" method="post">
	<input type="hidden" name="id" value="${crawlTask.id}"/>
	<table>
		<tr>
			<td colspan="3" class="tableHeadBg">
				<div align="center">管理抓取任务执行计划</div>
			</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">搜索引擎:</td>
			<td>
				<select name="searchEngineId">
					<c:forEach var="engine" items="${engines}">
						<c:choose>
							<c:when test="${crawlTask.searchEngineId == engine.id}">
								<option value="${engine.id}" selected="selected">${engine.name}</option>
							</c:when>
							<c:otherwise>
								<option value="${engine.id}">${engine.name}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">运行服务器地址:</td>
			<td>
				<select name="runServerIp">
					<c:forEach var="server" items="${servers}">
						<c:choose>
							<c:when test="${server.ip eq crawlTask.runServerIp}">
								<option value="${server.ip}" selected="selected">${server.ip}</option>
							</c:when>
							<c:otherwise>
								<option value="${server.ip}">${server.ip}</option>
							</c:otherwise>
						</c:choose>
						
					</c:forEach>
				</select>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">每次加载queryURL数量:</td>
			<td>
				<input type="text" class="InputCommon" name="findUrlSize" value="${crawlTask.findUrlSize}" />
			</td>
			<td><span style="color: red;">参考值：50</span></td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">线程池最小线程数:</td>
			<td>
				<input type="text" class="InputCommon" name="minThreadNum" value="${crawlTask.minThreadNum}" />
			</td>
			<td><span style="color: red;">参考值：2</span></td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">线程池最大线程数量:</td>
			<td>
				<input type="text" class="InputCommon" name="maxThreadNum" value="${crawlTask.maxThreadNum}" />
			</td>
			<td><span style="color: red;">参考值：4</span></td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">执行任务线程池队列长度:</td>
			<td>
				<input type="text" class="InputCommon" name="poolQueueSize" value="${crawlTask.poolQueueSize}" />
			</td>
			<td><span style="color: red;">参考值：500</span></td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">运行时间表达式:</td>
			<td>
				<input type="text" class="InputCommon" name="cronExp" value="${crawlTask.cronExp}" />
			</td>
			<td><span style="color: red;">参考值：</span></td>
		</tr>
		<tr class="tableAlternationBg1">
			<td>&nbsp;</td>
			<td>
				<input type="submit" value="提交"><input type="reset" value="重置">
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>
<br><a href="<c:url value="/index.jsp" />">返回首页</a><br>
</body>
</html>
