<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<title>小工具集-获取URL对应的HTML代码</title>
<style type="text/css">
.redline {
	background-color: #FCEFE7;
	border-right-width: 1px;
	border-left-width: 1px;
	border-right-style: solid;
	border-left-style: solid;
	border-right-color: #FE8929;
	border-left-color: #FE8929;
}
.font12{
	font-family: 宋体;
	font-size: 12px;
}
</style>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>
<form action="<c:url value="/admin/tools/html"/>" method="post">
	<table>
		<tr>
			<td colspan="2" class="tableHeadBg">
				<div align="center">小工具集</div>
			</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">URL:</td>
			<td>
				<input type="text" name="url" value="<c:out value="${url}" escapeXml="true"/>" size="80"/>
			</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">是否启用GZIP:</td>
			<td>
				<select name="isGzip">
					<c:if test="${isGzip == 1 || isGzip == null}">
						<option value="1" selected="selected">启　用</option>
						<option value="0" >不启用</option>
					</c:if>
					<c:if test="${isGzip == 0}">
						<option value="1" >启　用</option>
						<option value="0" selected="selected">不启用</option>
					</c:if>
				</select>
			</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td>&nbsp;</td>
			<td>
				<input type="submit" value="提交"><input type="reset" value="重置">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div class="redline font12">
					<c:out value="${htmlSource}" escapeXml="true"/>
				</div>
			</td>
		</tr>
	</table>
</form>

<br><a href="<c:url value="/index.jsp" />">返回首页</a><br>
</body>
</html>
