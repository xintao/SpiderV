<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<link href="<c:url value="/css/ui-lightness/jquery-ui-1.8.6.custom.css"/>" rel="stylesheet" type="text/css"  />
<script src="<c:url value="/js/jquery-1.4.2.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/jquery-ui-1.8.6.custom.min.js"/>" type="text/javascript"></script>
<title>查询指定日期运行日志</title>
<script type="text/javascript">
<!--
	$(document).ready(function() {
		//button ui
		$("#datepicker").datepicker();
		$("#datepicker").datepicker("option", "dateFormat", 'yy-mm-dd' );
	});
//-->
</script>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>
<form action="<c:url value="/admin/log/find"/>" method="post" >
	<table>
		<tr>
			<td colspan="3" class="tableHeadBg">
				<div align="center">查询指定日期运行日志</div>
			</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">日期:</td>
			<td>
				<input type="text" class="InputCommon" id="datepicker" name="spiderDate" />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td>&nbsp;</td>
			<td>
				<input type="submit" value="提交"><input type="reset" value="重置">
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>
</body>
</html>
