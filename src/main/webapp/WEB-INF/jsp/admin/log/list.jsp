<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet"
	type="text/css" />
<title>[${spiderDate}]-运行日志列表</title>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a>
<br>
<table>
	<tr>
		<td colspan="13" class="tableHeadBg">
			<div align="center">[${spiderDate}]-运行日志列表（按照搜索引擎ID、任务完成时间正排序）</div>
		</td>
	</tr>
	<tr class="tableTitleBg">
		<td width="10%">抓取日期</td>
		<td width="5%">网站ID</td>
		<td width="13%">网站名称</td>
		<td width="5%">搜索引擎ID</td>
		<td width="13%">搜索引擎名称</td>
		<td width="14%">IP</td>
		<td width="5%">提交查询总数量</td>
		<td width="5%">提交查询平均速度（a/s）</td>
		<td width="5%">数据获取总数量</td>
		<td width="5%">数据获取平均速度（a/s）</td>
		<td width="10%">任务开始时间</td>
		<td width="10%">任务完成时间</td>
		<td width="10%">数据库保存记录数</td>
	</tr>
	<c:forEach items="${logs}" var="log" varStatus="status" >
	<tr class="<c:if test="${status.count%2==0}">tableAlternationBg2</c:if><c:if test="${status.count%2==1}">tableAlternationBg1</c:if>">
		<td>${log.spiderDate}</td>
		<td>${log.websiteId}</td>
		<td>${log.websiteName}</td>
		<td>${log.searchEngineId}</td>
		<td>${log.searchEngineName}</td>
		<td>${log.ip}</td>
		<td>${log.queryCount}</td>
		<td>${log.querySpeed}</td>
		<td>${log.jobCount}</td>
		<td>${log.jobSpeed}</td>
		<td>${log.startTime}</td>
		<td>
		<c:choose>
			<c:when test="${log.needCheck}">
				<span style="color: red;">${log.finishTime}</span>
			</c:when>
			<c:otherwise>
				${log.finishTime}
			</c:otherwise>
		</c:choose>
		</td>
		<td>${log.savedJobCount}</td>
		
	</tr>
	</c:forEach>
</table>
<br>
<a href="<c:url value="/index.jsp" />">返回首页</a>
<br>
</body>
</html>
