<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<link href="<c:url value="/css/ui-lightness/jquery-ui-1.8.6.custom.css"/>" rel="stylesheet" type="text/css"  />
<script src="<c:url value="/js/jquery-1.4.2.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/jquery-ui-1.8.6.custom.min.js"/>" type="text/javascript"></script>
<title>删除指定网站的指定日期数据表</title>
<script type="text/javascript">
<!--
	$(document).ready(function() {
		//button ui
		$("#datepicker").datepicker();
		$("#datepicker").datepicker("option", "dateFormat", 'yy-mm-dd' );
	});
	
	function submitConfirm(){
		var	message="注意：您将执行drop数据流水日志表操作，确认吗？";
		return confirm(message);
	}
//-->
</script>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>
<form action="<c:url value="/admin/spider/drop"/>" method="post" onsubmit="return submitConfirm()" >
	<table>
		<tr>
			<td colspan="3" class="tableHeadBg">
				<div align="center">删除指定网站的指定日期数据表</div>
			</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">搜索引擎:</td>
			<td>
				<select name="searchEngineId">
					<c:forEach var="engine" items="${engines}">
						<option value="${engine.id}">${engine.name}</option>
					</c:forEach>
				</select>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">日期:</td>
			<td>
				<input type="text" class="InputCommon" id="datepicker" name="date" />
			</td>
			<td><span style="color: red;">指定日期后缀的数据日志表将被删除</span></td>
		</tr>
		<tr class="tableAlternationBg1">
			<td>&nbsp;</td>
			<td>
				<input type="submit" value="提交"><input type="reset" value="重置">
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>
<ul id="tableList">
	<c:forEach var="table" items="${tables}">
	<li>
		${table}
	</li>
	</c:forEach>
</ul>
</body>
</html>
