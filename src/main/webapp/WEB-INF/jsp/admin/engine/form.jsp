<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<title>管理搜索引擎</title>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>
<form action="<c:url value="/admin/engine"/>" method="post">
	<input type="hidden" name="id" value="${searchEngine.id}"/>
	<table>
		<tr>
			<td colspan="3" class="tableHeadBg">
				<div align="center">管理搜索引擎</div>
			</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel" width="20%">网站:</td>
			<td width="40%">
				<select name="websiteId">
					<option value="">--请选择--</option>
					<c:forEach var="mark" items="${marks}">
						<c:choose>
							<c:when test="${searchEngine.websiteId == mark.id}">
								<option value="${mark.id}" selected="selected">${mark.name}</option>
							</c:when>
							<c:otherwise>
								<option value="${mark.id}">${mark.name}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
			</td>
			<td width="40%">设定这个搜索引擎的所属网站，抓取到的数据也将标记成这个网站的，网站列表可通过外部系统维护，本系统加载一次并缓存，提供了刷新功能。</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">搜索引擎名:</td>
			<td>
				<input type="text" class="InputCommon" name="name" value="${searchEngine.name}"/>
			</td>
			<td>名称最好使用英文，此名称对日志分析，排查错误有很大帮助</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">引擎提交URL:</td>
			<td>
				<input type="text" class="InputCommon" size="70" name="baseUrl" value="${searchEngine.baseUrl}"/>
			</td>
			<td>一个搜索引擎数据请求或提交的基本URL部分，一般GET方式取“？”前面一段</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">页面编码:</td>
			<td>
				<select name="encode">
					<c:forEach items="${encodes}" var="encode">
						<c:if test="${encode.value == searchEngine.encode}">
							<option value="${encode.value}" selected="selected">${encode.name}</option>
						</c:if>
						<c:if test="${encode.value != searchEngine.encode}">
							<option value="${encode.value}" >${encode.name}</option>
						</c:if>
					</c:forEach>
				</select>
			</td>
			<td>网站默认的字符编码，一般不同特殊设定，系统自动会处理大多数情况，采用默认值即可。</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">URL参数编码:</td>
			<td>
				<select name="urlEncode">
					<c:forEach items="${encodes}" var="encode">
						<c:if test="${encode.value == searchEngine.urlEncode}">
							<option value="${encode.value}" selected="selected">${encode.name}</option>
						</c:if>
						<c:if test="${encode.value != searchEngine.urlEncode}">
							<option value="${encode.value}" >${encode.name}</option>
						</c:if>
					</c:forEach>
				</select>
			</td>
			<td>URL中文字符编码，一般设置UTF-8即可,部分设置GB2312。<span style="color: red;">注意：调整后需要重新生成搜索URL</span></td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">请求类型:</td>
			<td>
				<select name="method">
					<c:forEach items="${methods}" var="method">
						<c:if test="${method.value == searchEngine.method}">
							<option value="${method.value}" selected="selected">${method.name}</option>
						</c:if>
						<c:if test="${method.value != searchEngine.method}">
							<option value="${method.value}" >${method.name}</option>
						</c:if>
					</c:forEach>
				</select>
			</td>
			<td>HTTP请求方式，具体设定依据网站情况而定，系统将根据配置不同，采取不同的提交方式</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">是否支持GZIP:</td>
			<td>
				<select name="isGzip">
					<c:if test="${searchEngine.isGzip == 1 || searchEngine.isGzip == null}">
						<option value="1" selected="selected">启　用</option>
						<option value="0" >不启用</option>
					</c:if>
					<c:if test="${searchEngine.isGzip == 0}">
						<option value="1" >启　用</option>
						<option value="0" selected="selected">不启用</option>
					</c:if>
				</select>
			</td>
			<td>网站是否支持GZIP压缩，大多数网站均支持，可用系统提供的测试抓取页面接口，测试网站是否支持。</td>
		</tr>
		
		<tr class="tableAlternationBg2">
			<td class="tdlabel">生成搜索URL实现类:</td>
			<td>
				<c:choose>
					<c:when test="${fn:length(searchEngine.createQueryURLClass) == 0}">
						<input type="text" class="InputCommon" size="70" name="createQueryURLClass" value="com.yzq.os.spider.v.service.queryurl.impl.DefaultCreateQueryURLService"/>
					</c:when>
					<c:otherwise>
						<input type="text" class="InputCommon" size="70" name="createQueryURLClass" value="${searchEngine.createQueryURLClass}"/>
					</c:otherwise>
				</c:choose>
			</td>
			<td>将此搜索引擎的配置参数，通过排列组合形式生成待搜索URL。某些网站限定搜索结果，在此实现中需要将再次拼“非必要搜索参数”功能实现，一般默认即可。</td>
		</tr>
		
		<tr class="tableAlternationBg1">
			<td class="tdlabel">抓取调度前置实现类:</td>
			<td>
				<input type="text" class="InputCommon" size="70" name="beforeSpiderProcClass" value="${searchEngine.beforeSpiderProcClass}"/>
			</td>
			<td>例如：某些网站需要登录后才可以进行抓取，可将登录逻辑实现在这个类里,一般默认不填即可。</td>
		</tr>
		
		<tr class="tableAlternationBg2">
			<td class="tdlabel">抓取及翻页实现类:</td>
			<td>
				<select name="spiderTaskClass">
				<c:forEach var="spiderTask" items="${spiderTasks }">
					<c:choose>
						<c:when test="${spiderTask eq searchEngine.spiderTaskClass}">
							<option value="${spiderTask}" selected="selected">${spiderTask}</option>
						</c:when>
						<c:otherwise>
							<option value="${spiderTask}">${spiderTask}</option>
						</c:otherwise>
					</c:choose>
					
				</c:forEach>
				</select>
			</td>
			<td>实现具体的页面分析，翻页等操作，默认的实现类通过正则方式实现，但大多都用JSOUP实现。一般都要单独实现。</td>
		</tr>
		
		<tr class="tableAlternationBg1">
			<td class="tdlabel">补全抓取数据实现类:</td>
			<td>
				<input type="text" class="InputCommon" size="70" name="completionSpiderClass" value="${searchEngine.completionCrawlClass}"/>
			</td>
			<td>例如：一次普通抓取完毕后，通过后续规则生成新的搜索URL再抓取。</td>
		</tr>
		
		<tr class="tableAlternationBg2">
			<td class="tdlabel">搜索提交前休眠时间:</td>
			<td>
				<input type="text" class="InputCommon" name="sleepTime" value="${searchEngine.sleepTime}"/>
			</td>
			<td>单位：毫秒。用来减少蜘蛛对网站带来的压力。多线程时：如果为＂０＂，多个线程将同时访问网络，如果大于０，线程将进入同步网络访问（多个搜索引擎间抓取将不同步）</td>
		</tr>
		
		<tr class="tableAlternationBg1">
			<td>&nbsp;</td>
			<td>
				<input type="submit" value="提交"><input type="reset" value="重置">
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>
<br><a href="<c:url value="/index.jsp" />">返回首页</a><br>
</body>
</html>
