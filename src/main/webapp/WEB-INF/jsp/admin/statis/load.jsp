<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<link href="<c:url value="/css/ui-lightness/jquery-ui-1.8.6.custom.css"/>" rel="stylesheet" type="text/css"  />
<script src="<c:url value="/js/jquery-1.4.2.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/jquery-ui-1.8.6.custom.min.js"/>" type="text/javascript"></script>
<title>查看系统运行统计信息</title>
<script type="text/javascript">
	$(document).ready(function(){
		loadQueryUrl();
		loadCrawlJob();
		loadSpeed();
		progressbar();
	}); 
	
	function flush(){
		window.location.href="<c:url value="/admin/statis"/>";
	}
	
	var searchEngineMap=${engines};
	//query url do flag map
	var queryUrlDoFlagMap={
		"0":"等待处理",
		"1":"处理中",
		"2":"处理完毕",
		"301":"没有多值参数",
		"302":"没有限定条件",
		"303":"网络访问失败",
		"304":"HTML页面代码为空",
		"305":"没有数据页面",
		"306":"数据部分HTML代码为空",
		"307":"获取到的数据列表为空",
		"3":"其他未知错误"
	};
	//crawl job do flag map
	var crawljobDoFlagMap={
		"0":"等待处理",
		"1":"处理中",
		"2":"处理完毕"
	};
	
	function loadQueryUrl(){
		$.get("<c:url value="/admin/statis/queryurl"/>", function(data){
			 $("#queryurl").empty(); 
			 var div= $("#queryurl");
			 var table="<table>";
			 table+="<tr><td colspan=\"7\" class=\"tableHeadBg\">抓取URL分析</td></tr>";
			 $.each(data, function(i, rowData){
				 var css="tableAlternationBg1";
				 if(i%2==0){
					 css="tableAlternationBg2";
				 }
				 table+="<tr class=\""+css+"\"><td width=\"25%\">" + searchEngineMap[rowData[1]] + "</td><td width=\"15%\">表名：“" + rowData[0] + "”</td><td width=\"15%\">" + queryUrlDoFlagMap[rowData[2]] + "</td><td width=\"10%\">" + rowData[3] + "</td><td width=\"10%\">&nbsp;</td><td width=\"10%\">&nbsp;</td><td width=\"15%\">&nbsp;</td></tr>";
			 });
			 table+="</table>";
			 div.append(table);
		});
	}
	
	function progressbar(){
		$.get("<c:url value="/admin/statis/progressbar"/>", function(data){
			 $("#progressbar").empty(); 
			 var div= $("#progressbar");
			 $.each(data, function(i, rowData){
				var table="<table><tr><td width=\"15%\">"+searchEngineMap[rowData[0]]+"</td><td width=\"85%\"><div id=\"progressbar"+rowData[0]+"\"></div></td></tr></table>";
				div.append(table);
				$("#progressbar"+rowData[0]).progressbar({
					value: Number(rowData[3])
				});
			 });
			 $(".ui-progressbar-value").css("background-image","url(/SpiderVertical/css/ui-lightness/images/pbar-ani.gif)");
		});
	}
	
	function loadCrawlJob(){
		$.get("<c:url value="/admin/statis/spiderRecord"/>", function(data){
			 $("#crawljob").empty(); 
			 var div=$("#crawljob");
			 var table="<table>";
			 table+="<tr><td colspan=\"4\" class=\"tableHeadBg\">抓取数据数量信息(各网站最后保存数据表)</td></tr>";
			 $.each(data, function(i, rowData){
				 var css="tableAlternationBg1";
				 if(i%2==0){
					 css="tableAlternationBg2";
				 }
				 table+="<tr class=\""+css+"\"><td width=\"25%\">" + searchEngineMap[rowData[0]] + "</td><td width=\"20%\">表名：“" + rowData[3] + "”</td><td width=\"10%\">" + crawljobDoFlagMap[rowData[1]] + "</td><td width=\"10%\">" + rowData[2] + "</td></tr>";
			 });
			 table+="</table>";
			 div.append(table);
		}); 
	}
	
	function loadSpeed(){
		$.get("<c:url value="/admin/statis/loadSpeed"/>", function(msg){
			 $("#speed").empty(); 
			 var div=$("#speed");
			 $.each(msg,function(ip,data){
				var table="<table>";
				table+="<tr><td colspan=\"8\" class=\"tableHeadBg\">服务器["+ip+"]运行中任务</td></tr>";
				table+="<tr class=\"tableTitleBg\"><td width=\"25%\">搜索引擎</td><td width=\"15%\">开始时间</td><td width=\"15%\">当前时间</td><td width=\"8%\">已提交搜索数量</td><td width=\"8%\">提交搜索速度</td><td width=\"8%\">已抓数据数量</td><td width=\"8%\">抓数据速度</td><td width=\"13%\">操作</td></tr>";
				$.each(data, function(i, rowData){
					var css="tableAlternationBg1";
					if(i%2==0){
						css="tableAlternationBg2";
					}
					var btn="&nbsp;";
					if(rowData[7] == 'true'){
						btn="<button title=\"停止下次调度，完成正抓取的任务后才退出。\" onclick=\"stopThread('"+ip+"','"+rowData[0]+"')\">停止调度进程</button>";
					}
					table+="<tr class=\""+css+"\"><td>" + searchEngineMap[rowData[0]] + "</td><td>" + rowData[1] + "</td><td>" + rowData[2] + "</td><td>" + rowData[3] + "</td><td>" + rowData[4] + "</td><td>" + rowData[5] + "</td><td>" + rowData[6] + "</td><td width=\"10%\">"+btn+"</td></tr>";
				});
				table+="</table>";
				div.append(table);
			 });
			 
		}); 
	}
	/*5分钟自动刷新一次*/
	window.setInterval(loadSpeed,60000*5); 
	
	function stopThread(ip,searchEngineId){
		$.post("<c:url value="/admin/spider/stopRemote/"/>", { ip: ip, searchEngineId: searchEngineId },
			function(data) {
				alert(data);
				loadSpeed();
			}
		);
	}
</script>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>
<div>
	<div>
		<div id="speed">
			<img alt="Loading" src="<c:url value="/images/loading.gif"/>" width="100" height="100"/>
		</div>
	</div>
	</div>
		<div id="queryurl">
			<img alt="Loading" src="<c:url value="/images/loading.gif"/>" width="100" height="100"/>
		</div>
	<div>
	<div>
		<div id="progressbar">
			<img alt="Loading" src="<c:url value="/images/loading.gif"/>" width="100" height="100"/>
		</div>
	</div>
	<div>
		<div id="crawljob">
			<img alt="Loading" src="<c:url value="/images/loading.gif"/>" width="100" height="100"/>
		</div>
	</div>
	<div align="center">
		<button onclick="flush()" value="刷新数据">刷新数据</button>
	</div>
</div>
<br><a href="<c:url value="/index.jsp" />">返回首页</a><br>
</body>
</html>
